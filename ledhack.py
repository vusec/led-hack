#!/usr/bin/env python
import os

from kivy.app import App
from kivy.logger import Logger

from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.properties import BooleanProperty, StringProperty, NumericProperty, ListProperty

# from kivy.uix.widget import Widget
# from kivy.uix.label import Label

from kivy.garden.knob import Knob
from kivy.garden.multichoicetogglebutton import MultichoiceToggleButton
from kivy.garden.iconfonts import iconfonts, icon

ICONFONTS_CONFIG = [
    'default_font',
    os.path.join(os.path.dirname(iconfonts.__file__), 'fontawesome-webfont.ttf'),
    os.path.join(os.path.dirname(iconfonts.__file__), 'font-awesome.fontd')
]

class TopLayout(BoxLayout):
    pass

class AddressToggleButton(MultichoiceToggleButton):
    text = StringProperty('0')
    font_name = StringProperty('libs/envy_code_r.ttf')
    font_size = NumericProperty(20)
    background_color = ListProperty([0.0, 0, 0.0, 1.0])
    def on_release(self):
        if self.state == 'normal':
            self.text = '0'
        else:
            self.text = '1'
        # print(dir(self))

class FrequencyKnob(Knob):
    def on_knob(self, value):
        freq = (int(value)/self.step)*self.step
        self.parent.children[0].text = '%dHz' % freq

class LEDCommandInput(TextInput):
    font_name = StringProperty('libs/envy_code_r.ttf')
    multiline = BooleanProperty(True)
    def on_enter(instance, value):
        print('User pressed enter in', instance)

class LEDHackApp(App):
    def build(self):
        layout = TopLayout()
        return layout

if __name__ == '__main__':
    iconfonts.register(*ICONFONTS_CONFIG)
    LEDHackApp().run()